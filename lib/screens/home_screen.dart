import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:furniture_app/dummy/dummy_data.dart';
import 'package:furniture_app/helper/color_helper.dart';
import 'package:furniture_app/widgets/basket_widget.dart';
import 'package:furniture_app/widgets/collection_widget.dart';
import 'package:furniture_app/widgets/item_list_widget.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final newArrival = DummyData.newArrival;
    final collections = DummyData.collection;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.whiteDarker,
        elevation: 0,
        leadingWidth: 65,
        leading: Container(
          padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
          child: Container(
            decoration: BoxDecoration(
                color: ColorHelper.whiteLighter,
                borderRadius: BorderRadius.circular(10)),
            child: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.menu_outlined, color: Colors.black),
            ),
          ),
        ),
        actions: const [BasketWidget()],
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: [
          Container(
            margin: const EdgeInsets.only(top: 15, bottom: 20),
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('New arrival',
                    style: GoogleFonts.merriweather(
                        color: ColorHelper.secondary,
                        fontWeight: FontWeight.bold,
                        fontSize: 20)),
                IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.arrow_forward_outlined))
              ],
            ),
          ),
          SizedBox(
            height: 400,
            child: ListView(
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                children: List.generate(newArrival.length, (index) {
                  return ItemListWidget(item: newArrival[index]);
                })),
          ),
          Container(
            margin: const EdgeInsets.only(top: 35, bottom: 20),
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Collection',
                    style: GoogleFonts.merriweather(
                        color: ColorHelper.secondary,
                        fontWeight: FontWeight.bold,
                        fontSize: 20)),
                IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.arrow_forward_outlined))
              ],
            ),
          ),
          Container(
            height: 200,
            margin: const EdgeInsets.only(bottom: 20),
            child: ListView(
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                children: List.generate(collections.length, (index) {
                  return CollectionWidget(collection: collections[index]);
                })),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        height: 80,
        padding: const EdgeInsets.only(left: 25, right: 25, top: 0, bottom: 0),
        decoration: BoxDecoration(color: ColorHelper.white),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(width: 2, color: ColorHelper.primary)),
                ),
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {},
                      iconSize: 30,
                      color: ColorHelper.primary,
                      icon: const Icon(Icons.dashboard),
                    ),
                  ],
                )),
            Container(
                decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(width: 2, color: ColorHelper.white)),
                ),
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {},
                      iconSize: 30,
                      color: ColorHelper.grey,
                      icon: const Icon(Icons.chat_rounded),
                    ),
                  ],
                )),
            Container(
                decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(width: 2, color: ColorHelper.white)),
                ),
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {},
                      iconSize: 30,
                      color: ColorHelper.grey,
                      icon: const Icon(Icons.notifications_rounded),
                    ),
                  ],
                )),
            Container(
                decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(width: 2, color: ColorHelper.white)),
                ),
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {},
                      iconSize: 30,
                      color: ColorHelper.grey,
                      icon: const Icon(Icons.person_outlined),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
