import 'package:flutter/material.dart';
import 'package:furniture_app/dummy/dummy_data.dart';
import 'package:furniture_app/helper/color_helper.dart';
import 'package:furniture_app/models/item_model.dart';
import 'package:furniture_app/routes/AppRoutes.dart';
import 'package:furniture_app/widgets/basket_widget.dart';
import 'package:furniture_app/widgets/item_counter_widget.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class ItemDetailScreen extends StatelessWidget {
  const ItemDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int id = Get.arguments[0];
    final ItemModel item =
        DummyData.newArrival.firstWhere((element) => element.id == id);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.whiteDarker,
        elevation: 0,
        leadingWidth: 65,
        leading: Container(
          padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
          child: Container(
            decoration: BoxDecoration(
                color: ColorHelper.whiteLighter,
                borderRadius: BorderRadius.circular(10)),
            child: IconButton(
              onPressed: () {
                Get.toNamed(AppRoutes.DASHBOARD);
              },
              icon: const Icon(Icons.arrow_back, color: Colors.black),
            ),
          ),
        ),
        actions: const [BasketWidget()],
      ),
      body: Column(
        children: [
          Image.asset('assets/img/${item.itemPicture}', height: 400),
          Expanded(
            child: Container(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 20),
                decoration: BoxDecoration(
                  color: ColorHelper.white,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(item.itemName,
                                style: GoogleFonts.merriweather(
                                    color: ColorHelper.secondary,
                                    fontWeight: FontWeight.w900,
                                    fontSize: 25)),
                            const ItemCounterWidget()
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 40),
                          child: Text('Description',
                              style: GoogleFonts.merriweather(
                                  color: ColorHelper.secondary,
                                  fontWeight: FontWeight.w900,
                                  fontSize: 15)),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 15),
                          child: Text(DummyData.description,
                              textAlign: TextAlign.justify,
                              style: GoogleFonts.merriweather(
                                color: ColorHelper.secondary.withOpacity(0.5),
                                letterSpacing: 1,
                              )),
                        ),
                      ],
                    ),
                    Container(
                      height: 80,
                      margin: const EdgeInsets.only(bottom: 35),
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          gradient: LinearGradient(colors: [
                            ColorHelper.primary,
                            ColorHelper.primary,
                            ColorHelper.secondary.withOpacity(0.8),
                          ])),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: ColorHelper.white.withOpacity(0.3),
                                    borderRadius: BorderRadius.circular(15)),
                                padding: const EdgeInsets.all(10),
                                child: Icon(Icons.shopping_basket,
                                    color: ColorHelper.white),
                              ),
                              Container(
                                margin: const EdgeInsets.only(left: 20),
                                child: Text('Add to cart',
                                    style: GoogleFonts.merriweather(
                                        color: ColorHelper.white,
                                        fontWeight: FontWeight.w900,
                                        fontSize: 18)),
                              )
                            ],
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    left: BorderSide(
                                        width: 2,
                                        color: ColorHelper.white
                                            .withOpacity(0.3)))),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 20),
                            child: Text('\$${item.itemPrice.toString()}',
                                style: GoogleFonts.merriweather(
                                    color: ColorHelper.white,
                                    fontWeight: FontWeight.w900,
                                    fontSize: 25)),
                          )
                        ],
                      ),
                    )
                  ],
                )),
          )
        ],
      ),
    );
  }
}
