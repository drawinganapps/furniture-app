import 'package:flutter/material.dart';
import 'package:furniture_app/dummy/dummy_data.dart';
import 'package:furniture_app/helper/color_helper.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../widgets/item_cart_widget.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final carts = DummyData.carts;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.whiteDarker,
        elevation: 0,
        leadingWidth: 65,
        title: Text('Cart',
            style: GoogleFonts.merriweather(
                color: ColorHelper.secondary,
                fontWeight: FontWeight.w900,
                fontSize: 20)),
        centerTitle: true,
        leading: Container(
          padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
          child: Container(
            decoration: BoxDecoration(
                color: ColorHelper.whiteLighter,
                borderRadius: BorderRadius.circular(10)),
            child: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Icons.arrow_back, color: Colors.black),
            ),
          ),
        ),
        actions: [
          Container(
            width: 65,
            padding: const EdgeInsets.only(right: 15, bottom: 5, top: 5),
            child: Container(
                decoration: BoxDecoration(
                    color: ColorHelper.whiteLighter,
                    borderRadius: BorderRadius.circular(10)),
                child: IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.delete, color: Colors.black),
                )),
          )
        ],
      ),
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 20),
          ),
          Expanded(child: ListView(
            physics: const BouncingScrollPhysics(),
            padding: const EdgeInsets.only(left: 15, right: 15),
            children: List.generate(carts.length, (index) {
              return ItemCartWidget(cart: carts[index]);
            }),
          )),
          Container(
            padding: const EdgeInsets.only(left: 15, right: 15),
            height: 200,
            decoration: BoxDecoration(color: ColorHelper.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total (3 items) :',
                          style: GoogleFonts.merriweather(
                              color: ColorHelper.secondary.withOpacity(0.5),
                              fontWeight: FontWeight.w900,
                              fontSize: 18)),
                      Text('\$601',
                          style: GoogleFonts.merriweather(
                              color: ColorHelper.secondary,
                              fontWeight: FontWeight.w900,
                              fontSize: 20)),
                    ],
                  ),
                ),
                Container(
                  height: 80,
                  padding: const EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(colors: [
                        ColorHelper.primary,
                        ColorHelper.primary,
                        ColorHelper.secondary.withOpacity(0.8),
                      ])),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 25),
                        child: Text('Proceed to checkout',
                            style: GoogleFonts.merriweather(
                                color: ColorHelper.white,
                                fontWeight: FontWeight.w900,
                                fontSize: 18)),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: ColorHelper.primary,
                            borderRadius: BorderRadius.circular(15)),
                        padding: const EdgeInsets.all(10),
                        child: Icon(Icons.arrow_forward_outlined,
                            color: ColorHelper.white),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
