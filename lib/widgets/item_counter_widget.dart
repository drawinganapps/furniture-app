import 'package:flutter/material.dart';
import 'package:furniture_app/helper/color_helper.dart';
import 'package:google_fonts/google_fonts.dart';

class ItemCounterWidget extends StatelessWidget {
  const ItemCounterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(6),
      decoration: BoxDecoration(
          color: ColorHelper.grey,
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        mainAxisAlignment:
        MainAxisAlignment.spaceBetween,
        children: [
          Container(
              padding: const EdgeInsets.all(2),
              decoration: BoxDecoration(
                  color: ColorHelper.white,
                  borderRadius:
                  BorderRadius.circular(5)),
              margin:
              const EdgeInsets.only(right: 15),
              child: const Icon(Icons.remove, size: 20)),
          Text('1',
              style: GoogleFonts.merriweather(
                  color: ColorHelper.secondary,
                  fontWeight: FontWeight.w700,
                  fontSize: 18)),
          Container(
              padding: const EdgeInsets.all(2),
              decoration: BoxDecoration(
                  color: ColorHelper.white,
                  borderRadius:
                  BorderRadius.circular(5)),
              margin: const EdgeInsets.only(left: 15),
              child: const Icon(Icons.add, size: 20)),
        ],
      ),
    );
  }

}