import 'package:flutter/material.dart';
import 'package:furniture_app/helper/color_helper.dart';
import 'package:furniture_app/models/item_model.dart';
import 'package:furniture_app/routes/AppRoutes.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class ItemListWidget extends StatelessWidget {
  final ItemModel item;
  const ItemListWidget({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(AppRoutes.ITEM_DETAIL, arguments: [item.id]);
      },
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(left: 15, top: 35),
            width: 200,
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: ColorHelper.white, borderRadius: BorderRadius.circular(20)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: const EdgeInsets.only(top: 15),
                    decoration: BoxDecoration(
                        color: ColorHelper.secondary,
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(colors: [
                          ColorHelper.primary,
                          ColorHelper.primary,
                          ColorHelper.primary.withOpacity(0.9),
                        ])),
                    padding: const EdgeInsets.only(
                        top: 10, bottom: 10, left: 15, right: 15),
                    child: Text('New',
                        style: GoogleFonts.merriweather(
                            fontWeight: FontWeight.w900,
                            color: ColorHelper.white))),
                Container(
                  margin: const EdgeInsets.only(bottom: 25, top: 15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(item.itemName,
                          style: GoogleFonts.merriweather(
                              color: ColorHelper.secondary,
                              fontWeight: FontWeight.w700,
                              fontSize: 18)),
                      Row(
                        children: const [
                          Icon(Icons.star, color: Colors.orangeAccent, size: 15),
                          Icon(Icons.star, color: Colors.orangeAccent, size: 15),
                          Icon(Icons.star, color: Colors.orangeAccent, size: 15),
                          Icon(Icons.star, color: Colors.orangeAccent, size: 15),
                          Icon(Icons.star, color: Colors.orangeAccent, size: 15),
                        ],
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset('assets/logo/${item.itemBrandLogo}', width: 55, height: 25, fit: BoxFit.fill),
                    Container(
                      margin: const EdgeInsets.only(top: 5),
                    ),
                    Text('\$${item.itemPrice}',
                        style: GoogleFonts.merriweather(
                            color: ColorHelper.grey,
                            fontWeight: FontWeight.w500,
                            fontSize: 16)),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
              right: 10,
              child: Image.asset(
                'assets/img/${item.itemPicture}',
                fit: BoxFit.fill,
                height: 200,
                width: 180,
              ))
        ],
      ),
    );
  }
}
