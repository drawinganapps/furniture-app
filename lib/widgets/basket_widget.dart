import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:furniture_app/helper/color_helper.dart';
import 'package:furniture_app/routes/AppRoutes.dart';
import 'package:get/get.dart';

class BasketWidget extends StatelessWidget {
  const BasketWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 65,
      padding: const EdgeInsets.only(right: 15, bottom: 5, top: 5),
      child: Container(
          decoration: BoxDecoration(
              color: ColorHelper.whiteLighter,
              borderRadius: BorderRadius.circular(10)),
          child: Badge(
            position: const BadgePosition(top: 1, end: 3),
            badgeColor: Colors.orange,
            badgeContent: Text(
              '3',
              style: TextStyle(color: ColorHelper.white),
            ),
            child: IconButton(
              onPressed: () {
                Get.toNamed(AppRoutes.CART);
              },
              icon: const Icon(Icons.shopping_basket,
                  color: Colors.black),
            ),
          )),
    );
  }

}