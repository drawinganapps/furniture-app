import 'package:flutter/material.dart';
import 'package:furniture_app/helper/color_helper.dart';
import 'package:furniture_app/models/item_model.dart';
import 'package:furniture_app/widgets/item_counter_widget.dart';
import 'package:google_fonts/google_fonts.dart';

class ItemCartWidget extends StatelessWidget {
  final ItemModel cart;
  const ItemCartWidget({Key? key, required this.cart}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      padding: const EdgeInsets.all(15),
      height: 130,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), color: ColorHelper.white),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
                color: ColorHelper.grey.withOpacity(0.5),
                borderRadius: BorderRadius.circular(15)),
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(right: 15),
            child: Image.asset('assets/img/${cart.itemPicture}',
                width: 80, height: 80, fit: BoxFit.fill),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(cart.itemName,
                        style: GoogleFonts.merriweather(
                            color: ColorHelper.secondary.withOpacity(0.5),
                            fontWeight: FontWeight.w900,
                            fontSize: 18)),
                   Container(
                     margin: const EdgeInsets.only(top: 5),
                     child:  Image.asset('assets/logo/${cart.itemBrandLogo}', width: 50, height: 20, fit: BoxFit.fill),
                   )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('\$${cart.itemPrice}',
                        style: GoogleFonts.merriweather(
                            color: ColorHelper.secondary,
                            fontWeight: FontWeight.w900,
                            fontSize: 15)),
                    const ItemCounterWidget()
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
