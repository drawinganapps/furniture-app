import 'package:flutter/material.dart';

class CollectionWidget extends StatelessWidget {
  final String collection;
  const CollectionWidget({Key? key, required this.collection}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      margin: const EdgeInsets.only(left: 15),
      clipBehavior: Clip.antiAlias,
      child: Image.asset('assets/img/$collection', width: 400, height: 150, fit: BoxFit.fill),
    );
  }
}