import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(251,251,254, 1);
  static Color whiteLighter = const Color.fromRGBO(255,255,255, 1);
  static Color whiteDarker = const Color.fromRGBO(243,244,250, 1);
  static Color primary = const Color.fromRGBO(46,128,219, 1);
  static Color secondary = const Color.fromRGBO(5,30,71,1);
  static Color grey = const Color.fromRGBO(205,216,234, 1);
}