import 'package:flutter/material.dart';
import 'package:furniture_app/helper/color_helper.dart';

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: ColorHelper.whiteDarker,
  highlightColor: Colors.transparent,
  splashColor: Colors.transparent,
  fontFamily: 'Arial'
);
