import 'package:furniture_app/models/item_model.dart';

class DummyData {
  static List<ItemModel> newArrival = [
    ItemModel(
        1,
        'Muren Chair',
        'Ikea',
        'ikea.png',
        128,
        5,
        'kursi1.png'),
    ItemModel(
        2,
        'Stocksund Table',
        'Ace Hardware',
        'acehardware.png',
        199,
        5,
        'meja2.png'),
    ItemModel(
        3,
        'Vedbo Chair',
        'Informa',
        'informa.png',
        137,
        5,
        'kursi2.png'),
    ItemModel(
        4,
        'Koarp',
        'Informa',
        'informa.png',
        142,
        5,
        'kursi3.png'),
    ItemModel(
        5,
        'Nakas',
        'Ikea',
        'ikea.png',
        112,
        5,
        'meja1.png'),
  ];

  static List<ItemModel> carts = [
    newArrival[0],
    newArrival[1],
    newArrival[2]
  ];

  static List<String> collection = [
    'collection1.png',
    'collection2.png',
    'collection3.png',
    'collection4.jpeg',
  ];

  static String description = 'The robust construction is softened by the visible grains of the wood, giving the chair a warm and homey feeling. The straightforward design, rooted in chairs of the 50’s and 60’s, fits almost anywhere.';
}