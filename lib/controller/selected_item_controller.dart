import 'package:furniture_app/models/item_model.dart';
import 'package:get/get.dart';

class SelectedItemController extends GetxController {
  late ItemModel selectedItem;
  void changeSelected(ItemModel item)  {
    selectedItem = item;
    update();
  }
}
