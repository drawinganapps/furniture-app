import 'package:furniture_app/controller/selected_item_controller.dart';
import 'package:get/get.dart';

class SelectedItemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectedItemController>(() => SelectedItemController());
  }
}
