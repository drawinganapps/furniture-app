class AppRoutes {
  static const String DASHBOARD = '/';
  static const String ITEM_DETAIL = '/item-detail';
  static const String CART = '/cart';
}
