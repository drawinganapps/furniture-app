import 'package:flutter/cupertino.dart';
import 'package:furniture_app/screens/cart_screen.dart';
import 'package:furniture_app/screens/home_screen.dart';
import 'package:furniture_app/screens/item_detail_screen.dart';
import 'package:get/get.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const HomeScreen(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.ITEM_DETAIL,
        page: () => const ItemDetailScreen(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.CART,
        page: () => const CartScreen(),
        transition: Transition.rightToLeft),
  ];
}
