class ItemModel {
  final int id;
  final String itemName;
  final String itemBrandName;
  final int itemPrice;
  final double itemRatings;
  final String itemPicture;
  final String itemBrandLogo;

  ItemModel(this.id, this.itemName, this.itemBrandName, this.itemBrandLogo, this.itemPrice, this.itemRatings, this.itemPicture);
}
